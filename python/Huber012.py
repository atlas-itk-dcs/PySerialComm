#!/usr/bin/env python
#############################################
# Huber Intelligent Chiller Control 012  
#
# Carlos.Solans@cern.ch
# September 2021
#############################################

from time import sleep
from SerialCom import SerialCom

class Huber012:
    sc = None
    def __init__(self,portname,baudrate=1200,timeout=0.5):
        self.sc = SerialCom(portname,baudrate=baudrate,timeout=timeout)
        pass
    def setVerbose(self,enable):
        self.sc.setVerbose(True)
        pass
    def setRemote(self,enable):
        if enable: self.sc.write("REMOTE")
        else: self.sc.write("LOCAL")
        pass
    def setSetPoint(self,temperature):
        self.sc.write("SET %.1f" % temperature)
        pass
    def getSetPoint(self):
        return self.toFloat(self.sc.writeAndRead("SETPOINT?"))
    def getTemperature(self):
        return self.toFloat(self.sc.writeAndRead("INTERN?"))
    def toFloat(self,value):
        try: return float(value)
        except ValueError: print ("Not a float: %s"%value)
        return 0
