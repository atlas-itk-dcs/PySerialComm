#!/usr/bin/env python
#############################################
# Groningen Linear Stage
# Abhishek.Sharma@cern.ch
# November 2019
#############################################
from SerialCom import SerialCom

class GroningenLinearStage:

    sc = None
    def __init__(self,portname,baudrate=9600):
        self.sc = SerialCom(portname,baudrate=baudrate)
        pass

    def CalibrateX(self):
        print "Calibrating X Axis"
        cmd = "absp X 0xA"
        self.sc.write(cmd)
        pass

    def CalibrateY(self):
        print "Calibrating Y Axis"
        cmd = "absp Y 0xA"
        self.sc.write(cmd)
        pass

    def getPositionX(self):
        cmd="gpos X 0xA"
        return self.sc.writeAndRead(cmd)

    def getPositionY(self):
        cmd="gpos Y 0xA"
        return self.sc.writeAndRead(cmd)

    def moveToPositionAbsoluteX(self,position):
        position=float("%.2f"%position)
        print "Moving X to absolute pos: %.2f mm."%position
            cmd = "amov X %.2f 0xA"%position
            self.sc.writeAndRead(cmd)
            while self.sc.writeAndRead("stat X 0xA") != "OK":
                print "moving..."
                pass
            pass
        pass

    def moveToPositionAbsoluteY(self,position):
        position=float("%.2f"%position)
        print "Moving Y to absolute pos: %.2f mm."%position
            cmd = "amov Y %.2f 0xA"%position
            self.sc.writeAndRead(cmd)
            while self.sc.writeAndRead("stat Y 0xA") != "OK":
                print "moving..."
                pass
            pass
        pass

    def moveToPositionRelativeX(self,position):
        position=float("%.2f"%position)
        print "Moving X to relative pos: %.2f mm."%position
            cmd = "rmov X %.2f 0xA"%position
            self.sc.writeAndRead(cmd)
            while self.sc.writeAndRead("stat X 0xA") != "OK":
                print "moving..."
                pass
            pass
        pass

    def moveToPositionRelativeY(self,position):
        position=float("%.2f"%position)
        print "Moving Y to relative pos: %.2f mm."%position
            cmd = "rmov Y %.2f 0xA"%position
            self.sc.writeAndRead(cmd)
            while self.sc.writeAndRead("stat Y 0xA") != "OK":
                print "moving..."
                pass
            pass
        pass

    def getStatusX(self):
        return self.sc.writeAndRead("stat X 0xA")

    def getStatusY(self):
        return self.sc.writeAndRead("stat Y 0xA")

    def close(self):
        self.sc.close()
        pass

    pass
