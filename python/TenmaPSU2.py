#!/usr/bin/env python
#############################################
# Tenma power supply control
# Model 72-2795, 30V, 3A
# Florian.Haslbeck@cern.ch
# 
# March 2020
#############################################

from SerialCom import SerialCom
from time import sleep
    
class Tenma:
    sc = None
    def __init__(self,portname,baudrate=9600):
        self.sc = SerialCom(portname,baudrate = baudrate, dtr = False, rts = False, timeout = 1)
        self.verbose=False
        pass
    
    def setVerbose(self, enable):
        self.sc.setVerbose(enable)
        self.verbose=enable
        pass

    
    '''
    def getModel(self): #FIXME
        return self.sc.writeAndRead("*IDN?")
    '''
    '''
    def isEnabled(self, iOutput): #FIXME
        if self.sc.writeAndRead("OP%i?" % iOutput) == "1":
            return True
        else:
            return False
        pass
    '''
    '''
    def toFloat1(self,str): #FIXME
        ret=None
        if len(str)>2: ret=float(str[2:])
        return ret
    '''
    '''
    def toFloat2(self,str): #FIXME
        ret=None
        if len(str)>1: ret=float(str[:-1])
        return ret
    '''

    def setVoltageLimit(self,iOutput,fValue): #FIXME
        self.sc.write("OVP%i %f"%(iOutput,fValue))
        pass
    
    def setCurrentLimit(self,iOutput,fValue):  #FIXME
        self.sc.write("OCP%i %f"%(iOutput,fValue))
        pass
	
    def setVoltage(self,V):
        ch=1
        command="VSET%i:%.2f"%(ch,V)
        self.sc.write(command)
        sleep(0.5)
        self.sc.read()
        #return self.getVoltage()
        ## read voltage
        pass


    def setCurrent(self,iOutput,fValue):  #FIXME
        self.sc.write("I%i %f"%(iOutput,fValue))
        pass    
  


    def getVoltage(self):  #FIXME
        ch=1
        command = "VSET%i?"%(ch)
        self.sc.writeAndRead(command) #works sometimes
        #sleep(1)
        ##delay?? 0.5s??
        #return self.sc.read()
        #####return self.toFloat2(self.sc.writeAndRead("V%iO?"%iOutput))

    def getOutVoltage(self): #FIXME
        ch=1
        command = "VOUT%i?"%ch
        self.sc.writeAndRead(command) #works sometimes
        pass

    
    def getSetVoltage(self,iOutput):  #FIXME
        return self.toFloat1(self.sc.writeAndRead("V%i?"%iOutput))

    def getCurrent(self,iOutput):  #FIXME
        return self.toFloat2(self.sc.writeAndRead("I%iO?"%iOutput))
    
    def getVoltageLimit(self,iOutput):  #FIXME
        return self.toFloat2(self.sc.writeAndRead("OVP%i?"%iOutput))
    
    def getCurrentLimit(self,iOutput):  #FIXME
        return self.toFloat2(self.sc.writeAndRead("OCP%i?"%iOutput))

    def getSetCurrent(self,iOutput):  #FIXME
        return self.toFloat1(self.sc.writeAndRead("I%i?"%iOutput))

    def enableOutput(self,iOutput,bValue):  #FIXME
        if bValue == True:
            self.sc.write("OP%i 1"%iOutput)
            pass
        elif bValue == False:
            self.sc.write("OP%i 0"%iOutput)
            pass
        pass
    
    def getVersion(self):
        return self.sc.writeAndRead("*IDN?")
 
    def close(self):
        self.sc.close()

    def rampVoltage(self,iOutput,fsetNewValVolts,iSteps,isetDelay):   #FIXME
        vset=float(fsetNewValVolts)
        safe = True
        vold = self.getVoltage(iOutput)
        vold = round(vold,4)
        if abs(vset-vold)<1: 
            self.setVoltage(iOutput,vset)
            return
        s = 1
        iSteps = int(iSteps+1)
        vstp = float((vset-vold)/(iSteps-1))
        while s < (iSteps):
            v = self.getVoltage(iOutput)
            if v == -999:
                print "Error in voltage being set"
                safe = False 
                pass
            if safe == False: break
            self.setVoltage(iOutput,vstp*s+vold)
            sleep(isetDelay)
            print "Ramping Voltage: %.4f"%(vstp*s+vold)
            s += 1
            pass
        pass

    pass
