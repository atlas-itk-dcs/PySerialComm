#!/usr/bin/env python
#############################################
# Standa Axis Controller
# Florian.Haslbeck@cern.ch
# December 2019 - First version
# July 2021 - Abhi modified for SPS 
# April 2022 - Carlos added funcionalities
#############################################
# https://doc.xisupport.com/en/8smc4-usb/8SMCn-USB/Programming/Communication_protocol_specification.html

import io
import sys
import serial
from time import sleep

class StandaAxis:
    def __init__(self,
    		 port,
                 micronPerStep=2.5,
                 baudrate=115200,
                 stopbits=2,
                 timeout=0.4,
                 verbose = False
                 ):
        self.com = serial.Serial(port=port,baudrate=baudrate,stopbits=stopbits,timeout=timeout)
        self.verbose = False
        self.moving = False
        self.ishome = False
        self.micronPerStep = micronPerStep
        self.pos = 0.
        self.status = {}
        self.fw = {}
        self.gear = {}
        pass
    
    def setVerbose(self,enable=True):
        self.verbose = enable
        pass
    
    def bytesToInt(self,barr,start,size,signed=False):
        if size>16: size=16
        cnv = 0
        for b in range(size):
            cnv+=barr[b+start]<<((b)*8)
            pass
        if signed and (cnv & (1 << (8*size-1))): cnv -= 1 << 8*size
        return cnv
    
    def bytesToStr(self,barr,start,size):
        cnv = barr[start:start+size].decode("utf-8")
        return cnv
        
    def intToBytes(self,val,size,signed=False):
        hx='{0:0{1}x}'.format(val,size*2)
        if signed: hx='{0:0{1}x}'.format((val + (1<<(size*8)) & ((1<<(size*8))-1)),size*2)
        ba=bytearray()
        for i in range(0,size):
            ba+=bytearray.fromhex(hx[2*(size-i-1):2*(size-i)])
            pass
        return ba
    
    def write(self,cmd):
        self.com.write(cmd)
        pass

    def read(self, bytesize):
        rep = self.com.read(size=bytesize)
        if len(rep) != bytesize:
            print ("%s: Error!!!!"%self.axis)
            print ("%s: got \t%i / %i Bytes"%(self.axis,len(rep),bytesize))
        return rep
       
    def writeAndRead(self, cmd, bytesize = None):
        if self.verbose:
            print ("req: \t%s" %(":".join("{:02x}".format(c) for c in cmd))) #req in hex
            pass
        self.write(cmd)
        rep = self.read(bytesize)
        if self.verbose:
            print ("rep: \t%s" % (":".join("{:02x}".format(c) for c in rep))) #rep in hex
            pass
        return rep
        
    def getStatus(self):
        rep = self.writeAndRead('gets'.encode("utf-8"),54)
        self.status["Cmd"] = self.bytesToStr(rep,0,4)
        self.status["MoveState"] = self.bytesToInt(rep,4,1)
        self.status["MoveCmdState"] = self.bytesToInt(rep,5,1)
        self.status["PowerState"] = self.bytesToInt(rep,6,1)
        self.status["EncoderState"] = self.bytesToInt(rep,7,1)
        self.status["WindingsState"] = self.bytesToInt(rep,8,1)
        self.status["CurPos"] = self.bytesToInt(rep,9,4,True)
        self.status["uCurPos"] = self.bytesToInt(rep,13,2,True)
        self.status["EncPos"] = self.bytesToInt(rep,15,8,True)
        self.status["CurSpeed"] = self.bytesToInt(rep,23,4,True)
        self.status["uCurSpeed"] = self.bytesToInt(rep,27,2,True)
        self.status["Ipwr"] = self.bytesToInt(rep,29,2,True)
        self.status["Upwr"] = self.bytesToInt(rep,31,2,True)
        self.status["Iusb"] = self.bytesToInt(rep,33,2,True)
        self.status["Uusb"] = self.bytesToInt(rep,35,2,True)
        self.status["CurT"] = self.bytesToInt(rep,37,2,True)
        self.status["flags"] = self.bytesToInt(rep,39,4)
        self.status["gpioflags"] = self.bytesToInt(rep,43,4)
        self.status["CmdBuffFreeSpace"] = self.bytesToInt(rep,47,1)
        self.status["reserved"] = self.bytesToInt(rep,48,4)
        self.status["CRC"] = self.bytesToInt(rep,52,2)
        self.pos = ((self.status["CurPos"]+self.status["uCurPos"]/256.)*self.micronPerStep)/1000.
        self.moving = self.status["MoveCmdState"]&0x80
        self.ishome = self.status["flags"]%0x20
        return (self.status["Cmd"]=='gets')
        
    def printStatus(self):
        self.getStatus()
        print ("=========== getStatus() ===========")
        print ("Pos:      %(CurPos)10i [%(uCurPos)10i] (step [ustep])" % self.status)
        print ("Pos:      %10.3f (mm)" % self.pos)
        print ("Speed:    %(CurSpeed)10i [%(uCurSpeed)10i] (speed [uspeed])" % self.status)
        print ("Power:    %10.2f (V)" % (self.status["Upwr"]/100.))
        print ("Temp:     %10.1f (C)" % (self.status["CurT"]/10.))
        print ("Home:     %s" % ("yes" if self.ishome is True else "no"))
        print ("Moving:   %s" % ("yes" if self.moving is True else "no"))
        pass

    def getFirmwareVersion(self):
        rep = self.writeAndRead('gfwv'.encode("utf-8"),10)
        self.fw["Cmd"] = self.bytesToStr(rep,0,4)
        self.fw["Major"] = self.bytesToInt(rep,4,1)
        self.fw["Minor"] = self.bytesToInt(rep,5,1)
        self.fw["Release"] = self.bytesToInt(rep,6,2)
        self.fw["CRC"] = self.bytesToInt(rep,8,2)
        return (self.status["Cmd"]=='gfwv')

    def printFirmwareVersion(self):
        self.getFirmwareVersion()
        print ("====== getFirmwareVersion() =======")
        print ("Firmware: %i.%i" % (self.fw["Major"],self.fw["Minor"]))
        print ("Release:  %i" % self.fw["Release"])
        pass

    def getGearInfo(self):
        rep = self.writeAndRead('ggri'.encode("utf-8"),70)
        self.gear["Cmd"] = self.bytesToStr(rep,0,4)
        self.gear["Manufacturer"] = self.bytesToStr(rep,4,16)
        self.gear["PartNumber"] = self.bytesToStr(rep,20,24)
        self.gear["reserved"] = self.bytesToStr(rep,44,24)
        self.gear["CRC"] = self.bytesToInt(rep,68,2)
        return (self.status["Cmd"]=='ggri')

    def printGearInfo(self):
        self.getGearInfo()
        print ("========== getGearInfo() ==========")
        print ("Manufacturer: %s" % self.gear["Manufacturer"])
        print ("PartNumber:  %s" % self.gear["PartNumber"])
        pass

    def getMovementInfo(self):
        rep = self.writeAndRead('gmov'.encode("utf-8"),30)
        self.movement["Cmd"] = self.bytesToStr(rep,0,4)
        self.movement["Speed"] = self.bytesToStr(rep,4,4)
        self.movement["uSpeed"] = self.bytesToStr(rep,20,24)
        self.movement["Accel"] = self.bytesToStr(rep,20,24)
        self.movement["Decel"] = self.bytesToStr(rep,20,24)
        self.movement["AntiplaySpeed"] = self.bytesToStr(rep,20,24)
        self.movement["uAntiplaySpeed"] = self.bytesToStr(rep,20,24)
        self.movement["Reserved"] = self.bytesToStr(rep,20,24)
        self.movement["CRC"] = self.bytesToInt(rep,28,2)
        return (self.status["Cmd"]=='gmov')
    
    def printMovementInfo(self):
        self.getMovementInfo()
        print ("=========== getMovementInfo() =======")
        print ("Speed: %i" % self.movement["Speed"])
        print ("uSpeed: %i" % self.movement["Speed"])
        print ("Accel: %i" % self.movement["Speed"])
        print ("Decel: %i" % self.movement["Speed"])
        print ("AntiplaySpeed: %i" % self.movement["Speed"])
        print ("uAntiplaySpeed: %i" % self.movement["Speed"])
        pass

    def setMovementInfo(self):
        cmd='smov'.encode("utf-8")
        cmd+=self.intToBytes(self.speed,4)
        cmd+=self.intToBytes(self.uspeed,1)
        cmd+=self.intToBytes(self.acceleration,2)
        cmd+=self.intToBytes(self.decceleration,2)
        cmd+=self.intToBytes(self.antiplayspeed,4)
        cmd+=self.intToBytes(self.uantiplayspeed,1)
        cmd+=self.intToBytes(0,10)
        crc=self.crc16(cmd[4:])
        cmd+=self.intToBytes(crc,2)
        rep = self.writeAndRead(cmd,4)
        return (self.bytesToStr(rep,0,4)=="smov")
        
    def waitForStop(self):
        while self.isMoving():
            sleep(.1)
            pass
        if self.verbose: print('STOPPED')
        pass
        
    def isMoving(self):
        self.getStatus()
        return self.moving

    def moveLeft(self):
        self.writeAndRead('left'.encode("utf-8"),4)
        pass

    def moveRight(self):
        self.writeAndRead('rigt'.encode("utf-8"),4)
        pass

    def stop(self):
        self.writeAndRead('stop'.encode("utf-8"),4)
        pass
    
    def mmToStep(self,mm):
    	if "x" in self.axis: return int(mm * 404116/100.)
    	if "y" in self.axis: return int(mm * 404116/100.)#    	if "y" in self.axis: return int(mm * 402097/100.)
    	if "z" in self.axis: return int(mm * 404116/100.)#    	if "z" in self.axis: return int(mm * 390000/100.)
    
    def stepToMm(self,step):
        if "x" in self.axis: return (step*1.)/(50./404116)	
        if "y" in self.axis: return (step*1.)/(50./404116)#        if "y" in self.axis: return (step*1.)/(100./402097)
        if "z" in self.axis: return (step*1.)/(50./404116)#        if "z" in self.axis: return (step*1.)/(100./390000)
    	
    def moveAbsStep(self,pos,upos):
        cmd="move".encode("utf-8")
        cmd+=self.intToBytes(int(pos),4,True)
        cmd+=self.intToBytes(int(upos),2,True)
        cmd+=self.intToBytes(0,6)
        crc=self.crc16(cmd[4:])
        cmd+=self.intToBytes(crc,2)
        rep=self.writeAndRead(cmd,4)
        return (self.bytesToStr(rep,0,4)=="move")

    def moveAbs(self, mm):
        pos,rst = divmod(mm*1000,self.micronPerStep)
        return self.moveAbsStep(int(pos),int(rst*256))

    def moveRelStep(self,delta,udelta):
        cmd="movr".encode("utf-8")
        cmd+=self.intToBytes(int(delta),4,True)
        cmd+=self.intToBytes(int(udelta),2,True)
        cmd+=self.intToBytes(0,6)
        crc=self.crc16(cmd[4:])
        cmd+=self.intToBytes(crc,2)
        rep=self.writeAndRead(cmd,4)
        return (self.bytesToStr(rep,0,4)=="movr")

    def moveRel(self, mm):
        delta,rst = divmod(mm*1000,self.micronPerStep)
        return self.moveRelStep(int(delta),int(rst*256))
        
    def getPos(self):
        self.getStatus()
        return self.pos
        
    def crc16(self,pbuf): 
        crc = 0xffff					
        carry_flag = 0
        a = 0
        n=len(pbuf)
        for i in range(0,n):
            crc = crc ^ pbuf[i]	# bitwise XOR : x ^ y : x if y=0, complement x if y=1
            for j in range(0,8):
                a = crc
                carry_flag = a & 0x0001	# bitwise AND: 	x & y : 1 if x AND y both 1, else 0 
                crc = crc >> 1		# rightshift (crc = (crc dived by 2**1))
                if (carry_flag == 1 ):
                    crc = crc ^ 0xa001
                    pass
                pass
            pass
        return crc
       
    def checkCRC(self,rep):
        correctCRC = False
        l = len(rep)
        if l > 4:
            calCRC=self.crc16(rep[4:-2])  			#calculate crc
            recCRC=self.fromString(rep,len(rep)-2,2) 	#received crc
            correctCRC = (calCRC == recCRC)
            pass		
        elif l == 4:
            correctCRC = True
            pass
        elif l < 4:
            print("Answer shorter than 4 Bytes!!")
            pass
        return correctCRC


