#!/usr/bin/env python
import os
import sys
import json
import socket 

class PSURemoteController:
    
    def __init__(self, host, port):
        self.host=host
        self.port=port
        self.sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(3)
        self.connected=False
        self.verbose=False
        pass

    def setVerbose(self,enable):
        self.verbose=enable
        pass

    def open(self):
        print("Connecting to server: %s:%i" % (self.host, self.port))
        try:
            self.sock.connect((self.host, self.port))
            self.connected=True
        except:
            print("Could not connect to server")
            self.connected=False
        pass

    def close(self):
        self.sock.close()
        pass

    def getPsus(self):
        ret=self.sendCmd("showConnectedPSUs")
        for p in ret:
            ret[p]["Name"]=p
            pass
        return ret
    
    def sendCmd(self,cmd,psu=None,data=None):
        if self.verbose: print("Encode message")
        req={"cmd":cmd}
        if psu: req["PSUName"]=psu
        if data: req["data"]=data
        s_req = json.dumps(req)
        n_req = "%08x" % len(s_req)
        if self.verbose: print("Send len: %s" % n_req)
        self.sock.send(n_req)
        if self.verbose: print("Send msg: %s" % s_req)
        self.sock.send(s_req)
        if self.verbose: print("Wait for reply...")
        n_rep = int(self.sock.recv(8),16)
        if self.verbose: print("Recv len: %i" % n_rep)
        s_rep = self.sock.recv(n_rep)
        if self.verbose: print("Recv msg: %s" % s_rep)
        if self.verbose: print("Decode message")
        rep=json.loads(s_rep)
        if "not Reply" in rep: return False
        return rep["Reply"]
    
    def getSetVoltage(self, psu):
        return self.sendCmd("getSetVoltage",psu)
    
    def getVoltageLimit(self, psu):
        return self.sendCmd("getVoltageLimit",psu)

    def getCurrentLimit(self, psu):
        return self.sendCmd("getCurrentLimit",psu)

    def getVoltage(self, psu):
        return self.sendCmd("getVoltage",psu)

    def getCurrent(self, psu):
        return self.sendCmd("getCurrent",psu)

    def isEnabled(self, psu):
        return self.sendCmd("isEnabled",psu)

    def setOutput(self, psu, val):
        cmd="enablePSU" if val else "disablePSU"
        return self.sendCmd(cmd,psu)
    
    def setVoltage(self, psu, val):
        return self.sendCmd("setVoltage",psu,val)
    
    def rampVoltage(self, psu, val):
        return self.sendCmd("rampVoltage",psu,val)

    def setVoltageLimit(self, psu, val):
        return self.sendCmd("setVoltageLimit",psu,val)

    def setCurrentLimit(self, psu, val):
        return self.sendCmd("setCurrentLimit",psu,val)
    
    pass
