#!/usr/bin/env python3
# MultiComp digital scale 800 g x 0.01
#
# florian.haslbeck@cern.ch
# June 2023

# from SerialCom import SerialCom
from operator import imod
import serial
from time import sleep
import re


class MultiComp:
    sc = None

    def __init__(self, portname, baudrate=9600):
        # self.sc = SerialCom(portname,baudrate = baudrate, bytesize=8, parity='N')
        self.portname = portname
        self.baudrate = baudrate
        self.bytesize = 8
        self.parity = 'N'
        self.open(portname)
        pass

    def setVerbose(self, enable):
        self.sc.setVerbose(enable)
        pass

    def open(self, portname):
        self.sc = serial.Serial(port=portname,
                                baudrate=self.baudrate, bytesize=self.bytesize, parity=self.parity)

    def close(self):
        self.sc.close()

    def reconnect(self):
        """ reopen the port """
        self.close()
        sleep(2)
        self.open(self.portname)
        sleep(1)

    def read_weight_grams(self):
        """ read the output in grams (if set to this unit!)
            output format 1 when set to continuous output
            every reading output is 3 lines, first 1 is data, 2 others are empty
        """

        def _extract_weight(string):
            """ extract the weight in grams from the output of the scale
                for faulty readings return -12345
                example: 'ST,GS-       0.05 g '
            """
            

            pattern = r"ST,(NT|GS)([+-])\s+([\d.]+)\s+g"
            match = re.search(pattern, string)
            if match:
                category = match.group(1)  # This will capture either  'NT', or 'GS'
                sign = match.group(2)      # This will capture the sign character '+' or '-'
                weight = float(match.group(3))  # This will capture the weight as a float value
                print("Category", category)
                if category == 'NT': print("Measured net weight!")
                if category == 'GS': print("Measured gross weight!")
                if sign == '+':
                    return weight
                elif sign == '-':
                    return -1. * weight
                else:
                    print("Read invalid sign: %s"%string)
                    return -12345.
            else:
                print("Reading was %s"%string)
                return -12345.

        # flush the buffer
        self.sc.flushInput()

        # read 3 lines at a time to get a reading
        all_rep = ''
        for i in range(3):
            all_rep += str(self.sc.readline())

        # convert to weight and sign
        weight_g = _extract_weight(all_rep)
        return weight_g

    def tare(self):
        """ tare the scale """
        self.sc.write("T\r\n".encode('utf-8'))  # send T<cr><lf>


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help='port', default='/dev/ttyUSB0')
    args = parser.parse_args()

    mc = MultiComp(args.port)

    for i in range(10):
        print(mc.read_weight_grams())
        if i % 2 == 0: 

            print("Tared!")
            mc.tare()
        input("Press any key to continue")
