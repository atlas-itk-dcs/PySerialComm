#!/usr/bin/env python
#############################################
# Newport motion controller communication 
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################
# If Newport shows axis distance exceeded,
# make that axis move in the opposite direction
# an arbitrary amount, once reached, switch off
# and on Newport. Limitation will disappear.
#
# Default settings
# baudrate: 19200
# bits: 8
# parity: N
# stopbits: 1
#############################################

from SerialCom import SerialCom
import time

class Newport:
    sc = None
    def __init__(self,portname):
        self.sc = SerialCom(portname)
        pass

    def setVerbose(self, enable):
        self.sc.setVerbose(enable)
        pass
    
    def enableAllMotors(self):
        for i in range(1,4):
            self.sc.write("%i MO"%i)
            pass
        pass
    
    def disableAllMotors(self):
        for i in range(1,4):
            self.sc.write("%i MF"%i)
            pass
        pass
    
    def goHome(self):
        for i in range(1,4,1):
            self.sc.write("1PA"+"-25")
            self.sc.write("2PA"+"-12")
            self.sc.write("3PA"+"-5") #slower speed necessary, CHANGE IT
            pass
        pass
    
    def getCurrentPosition(self,iAxis):
        try: 
            return float(self.sc.writeAndRead("%iTP"%iAxis))
        except:
            return self.getCurrentPosition(iAxis)
        pass
    
    def moveAbsolute(self,iAxis,fValue):
        return self.sc.write("%iPA%.4f"%(iAxis,fValue))
        
    def moveRelative(self,iAxis,fValue):
        return self.sc.write("%iPR%.4f"%(iAxis,fValue))

    def stopMotion(self,iAxis):
        self.sc.write("%iST"%iAxis)
        pass
    
    def getVelocity(self,iAxis):
        return self.sc.writeAndRead("%iVA?"%iAxis)
                    
    def setVelocity(self,iAxis,fValue):
        return self.sc.write("%iVA%.1f"%(iAxis,fValue)) 

    def getDeceleration(self,iAxis):
        return self.sc.writeAndRead("%iAG?"%iAxis)

    def setDeceleration(self,iAxis,fValue):
        return self.sc.write("%iAG%.1f"%(iAxis,fValue))  

    def getAcceleration(self,iAxis):
        return self.sc.writeAndRead("%iAC?"%iAxis)
        
    def setAcceleration(self,iAxis,fValue):
        return self.sc.write("%iAC%.1f"%(iAxis,fValue))        

    def close(self):
        self.sc.close()
        pass
    
    # restart the newport controller
    # param iAxis axis to reset
    def resetController(self,iAxis): 
        return self.sc.writeAndRead("%iRS"%iAxis)

    def abortMotion(self):
        return self.sc.write("AB")

    #Below methods require Python "sleep". 
    #Otherwise Python overrides delayed Newport cmd executions.
    #time in milliseconds #removed axis option (no need) 
    def waitForStop(self,iAxis,iTimeMilliseconds = 0): 
        #self.sc.write("%iWS%i"%(iAxis,iTimeMilliseconds)) 
        t0=time.time()
        rt="0"
        while rt!="1":
            time.sleep(1)
            rt=self.sc.writeAndRead("%iMD?"%(iAxis))
            #print "Newport motion is '%s'"%rt
            t1=time.time()
            if t1-t0 > 30: break
            pass
        time.sleep(.3)
        pass

    # wait for position of a given axis
    # param iAxis axis to move
    # param fValue time in milliseconds
    def waitForPosition(self,iAxis,fValue): 
        return self.sc.write("%iWP%f"%(iAxis,fValue)) 

    def wait(self,fDelay): #time in milliseconds
        return self.sc.write("WT%f"%fDelay) #try wait for stop -> WS

    def getErrNum(self):
        return self.sc.writeAndRead("TE?")

    def getError(self):
        return self.sc.writeAndRead("TB?")
