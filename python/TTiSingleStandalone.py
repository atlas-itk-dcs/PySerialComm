#!/usr/bin/env python
#############################################
# TTI single power supply control
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Sascha.Dungs@cern.ch
# September 2016
#############################################

import serial
import io
from SerialCom import SerialCom

class TTiSingle:
    
    def __init__(self,portname):
        ## self.com = serial.Serial()
##         self.com.port=portname
##         self.com.baudrate=9600
##         self.com.bytesize=8
##         self.com.parity='N'
##         self.com.stopbits=1
##         self.com.dtr=False
##         self.com.rts=False
##         self.com.xonxoff=True
##         self.com.timeout=0.1
##         self.com.open()
        self.com = serial.Serial(port=portname,
                                 baudrate=9600,
                                 timeout=0.1,
                                 bytesize=8,
                                 parity='N',
                                 stopbits=1,
                                 xonxoff=True)
        self.com.dtr=False
        self.com.rts=False
        self.sio = io.TextIOWrapper(io.BufferedRWPair(self.com,self.com),
                                    errors='ignore')
        self.trm = '\r\n'
        self.verbose = False
        for k in self.com.getSettingsDict():
            print k, self.com.getSettingsDict()[k]
            pass
        print "Open: ",self.com.is_open
        print "rts : ",self.com.rts
        print "dtr : ",self.com.dtr
        pass
    
    def read(self):
        s=None
        try:
            print "reading"
            s=self.sio.readline().replace("\r","").replace("\n","")
            if self.verbose: print "SerialCom::read : %s" % s
            return s
        except Exception as ex:
            print ex
            if self.verbose: print "SerialCom::read : TIMEOUT"
            pass
        return s

    def write(self,cmd):
        cmd=cmd.replace("\r","").replace("\n","")+self.trm
        if self.verbose: print "SerialCom::write : %s" %cmd
        self.sio.write(unicode(cmd))
        self.sio.flush()
        pass

    def writeAndRead(self,cmd):
        self.write(cmd)
        return self.read()

    def toFloat(self,str):
        ret=None
        if len(str)>2: ret=float(str[2:])
        return ret
    
    def setVoltageLimit(self,fValue):
        self.write("OVP %f"%fValue)
        pass
    
    def reset(self):
        self.write("*RST")
        pass
    
    def setCurrentLimit(self,fValue):
        self.write("OCP %f"%fValue)
        pass
    
    def setVoltage(self,fValue):
        self.write("V %f"%fValue)
        pass
    
    def getVoltage(self):
        return self.toFloat(self.sc.writeAndRead("V?"))
    
    def getPower(self):
        str=self.writeAndRead("POWER?")
        ret=None
        if len(str)>1: ret=float(str[:-1])
        return ret

    def setCurrent(self,fValue):
        self.write("I %f"%fValue)
        pass
    
    def getCurrent(self):
        return self.toFloat(self.sc.writeAndRead("I?"))
    
    def enableOutput(self,bValue):
        self.write("OP %i" % (1 if bValue==True else 0))
        pass
    
    def lockNAM(self):
        self.write("LNA")
        pass
    
    def enableNAM(self):
        self.write("UDC")
        pass
    
    def close(self):
        self.com.close()
        pass
    
    def setVerbose(self,enable):
        self.verbose=enable
        pass
    
    def getVersion(self):
        return self.writeAndRead("*IDN?")
        
    pass
