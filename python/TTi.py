#!/usr/bin/env python
#############################################
# TTI power supply control
# Model PL303QMD-P
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# February 2016
#############################################

from SerialCom import SerialCom
from time import sleep
    
class TTi:
    sc = None
    def __init__(self,portname,baudrate=9600):
        self.sc = SerialCom(portname,baudrate = baudrate)
        pass
    
    def setVerbose(self, enable):
        self.sc.setVerbose(enable)
        pass
    
    def getModel(self):
        return self.sc.writeAndRead("*IDN?")
    
    def isEnabled(self, iOutput):
        if self.sc.writeAndRead("OP%i?" % iOutput) == "1":
            return True
        else:
            return False
        pass
    
    def toFloat1(self,str):
        ret=None
        if len(str)>2: ret=float(str[2:])
        return ret

    def toFloat2(self,str):
        ret=None
        if len(str)>1: ret=float(str[:-1])
        return ret

    def setVoltageLimit(self,iOutput,fValue):
        self.sc.write("OVP%i %f"%(iOutput,fValue))
        pass
    
    def setCurrentLimit(self,iOutput,fValue):
        self.sc.write("OCP%i %f"%(iOutput,fValue))
        pass
	
    def setVoltage(self,iOutput,fValue):
        self.sc.write("V%i %f"%(iOutput,fValue))
        pass
    
    def setCurrent(self,iOutput,fValue):
        self.sc.write("I%i %f"%(iOutput,fValue))
        pass    
  
    def getVoltage(self,iOutput):
        return self.toFloat2(self.sc.writeAndRead("V%iO?"%iOutput))
    
    def getSetVoltage(self,iOutput):
        return self.toFloat1(self.sc.writeAndRead("V%i?"%iOutput))

    def getCurrent(self,iOutput):
        return self.toFloat2(self.sc.writeAndRead("I%iO?"%iOutput))
    
    def getVoltageLimit(self,iOutput):
        return self.toFloat2(self.sc.writeAndRead("OVP%i?"%iOutput))
    
    def getCurrentLimit(self,iOutput):
        return self.toFloat2(self.sc.writeAndRead("OCP%i?"%iOutput))

    def getSetCurrent(self,iOutput):
        return self.toFloat1(self.sc.writeAndRead("I%i?"%iOutput))

    def enableOutput(self,iOutput,bValue):
        if bValue == True:
            self.sc.write("OP%i 1"%iOutput)
            pass
        elif bValue == False:
            self.sc.write("OP%i 0"%iOutput)
            pass
        pass
    
    def getVersion(self):
        return self.sc.writeAndRead("*IDN?")

    def close(self):
        self.sc.close()

    def rampVoltage(self,iOutput,fsetNewValVolts,iSteps,isetDelay):
        vset=float(fsetNewValVolts)
        safe = True
        vold = self.getVoltage(iOutput)
        if not self.isEnabled(iOutput):
            vold = self.getSetVoltage(iOutput)
            pass
        vold = round(vold,4)
        if abs(vset-vold)<1: 
            self.setVoltage(iOutput,vset)
            return
        s = 1
        iSteps = int(iSteps+1)
        vstp = float((vset-vold)/(iSteps-1))
        while s < (iSteps):
            v = self.getVoltage(iOutput)
            if v == -999:
                print("Error in voltage being set")
                safe = False 
                pass
            if safe == False: break
            self.setVoltage(iOutput,vstp*s+vold)
            sleep(isetDelay)
            print("Ramping Voltage: %.4f"%(vstp*s+vold))
            s += 1
            pass
        pass

    pass
