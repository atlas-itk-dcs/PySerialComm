#!/usr/bin/env python
import IselLES5
import argparse
import os

parser=argparse.ArgumentParser()
parser.add_argument("-px","--portx",help="USB port", default="/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907R6YR-if00-port0")
parser.add_argument("-py","--porty",help="USB port", default="/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907R6YU-if00-port0")
parser.add_argument("-cy", "--cy", help="calibrate Y axis", action="store_true")
parser.add_argument("-cx", "--cx", help="calibrate X axis", action="store_true")
parser.add_argument("-rx", "--rx", help="move X by mm", type=float)
parser.add_argument("-ry", "--ry", help="move Y by mm", type=float)
parser.add_argument("-ax", "--ax", help="move to absolute position X", type=float)
parser.add_argument("-ay", "--ay", help="move to absolute position Y", type=float)

args=parser.parse_args()

motorx = IselLES5.IselLES5(args.portx)
motory = IselLES5.IselLES5(args.porty)

motorx.configureController()
motory.configureController()
motory.invertAxis(True)

if args.cx: 
    print("Calibrate X axis")
    motorx.runReferenceRun()
    print("X axis calibrated")
    pass
if args.cy: 
    print("Calibrate Y axis")
    motory.runReferenceRun()
    print("Y axis calibrated")
    pass

if (args.ay is not None and args.ry is not None):
    print("Too many options given for Y axis")
elif args.ay is not None:
    print ("Y axis position [mm]: %s"% motory.getCurrentPosition())
    motory.moveToPositionAbsolute(args.ay)
elif args.ry is not None: 
    print ("Y axis position [mm]: %s"% motory.getCurrentPosition())
    motory.moveToPositionRelative(args.ry)

if (args.ax is not None and args.rx is not None):
    print("Too many options given for X axis")
elif args.ax is not None:
    print ("X axis position [mm]: %s"% motorx.getCurrentPosition())
    motorx.moveToPositionAbsolute(args.ax)
elif args.rx is not None:
    print ("X axis position [mm]: %s"% motorx.getCurrentPosition())
    motorx.moveToPositionRelative(args.rx)

print ("X axis position [mm]: %s"% motorx.getCurrentPosition())
print ("Y axis position [mm]: %s"% motory.getCurrentPosition())

print("Have a nice day")
